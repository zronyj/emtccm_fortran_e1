program exercise1

! -----------------------------------------------------------------------------------------
! This program does several matrix-vector exercises ---------------------------------------
! -----------------------------------------------------------------------------------------
!
! Inputs:
! - A matrix stored in a file called *matrix.dat*
! - A vector stored in a file called *vec.inp*
!
! Outputs:
! - Individual files for every exercise carried out by the program. Please see README.txt
!
! Special considerations:
! - I thought of using LAPACK for the matrix multiplications, but ...
!   https://stackoverflow.com/questions/31494176/will-fortrans-matmul-make-use-of-mkl-if-i-include-the-library
! - Reading and writing from and to files made the code seem more cluttered and inefficient.
!   Therefore, I created a module and sent all IO operations to the module.
! -----------------------------------------------------------------------------------------

! Using a module to separate the IO tasks and make the code readable and understandable ---
use rony_io, only : read_mat, read_vec, write_mat, write_vec, write_num, write_int

implicit none

! Declaring variables ---------------------------------------------------------------------
integer :: columnas, filas, dimvec, stat, i, j
character(len=255) :: buf
character(len=1023) :: cwd, results_dir
real(kind=4) :: traza, no_traza, col_impares, suma_mat_b
real(kind=4), allocatable :: input_vec(:), result_vec(:), matrixA(:,:), matrixB(:,:)
real(kind=4), allocatable :: matrix2(:,:), tmatrixA(:,:), tAxmatrixA(:,:), point_vector(:)
real(kind=4), allocatable :: tAxmatrixB(:,:), max_vector(:)

! Find out where the program is located and create subfolder ------------------------------
call getcwd(cwd)
results_dir = trim(cwd) // '/results'
call system('mkdir ' // results_dir)

! Loading matrix A ------------------------------------------------------------------------
call read_mat('matrix.dat', matrixA)
columnas = size(matrixA, 1)
filas = size(matrixA, 2)

! Loading the vector ----------------------------------------------------------------------
call read_vec('vec.inp', input_vec)
dimvec = size(input_vec)

! Memory allocation for the matrices and vectors ------------------------------------------
allocate(tmatrixA(filas, columnas)) ! Transpose
allocate(tAxmatrixA(columnas,columnas)) ! Transpose multiplication by matrix
allocate(matrixB(columnas-1,filas)) ! Matrix B
allocate(point_vector(filas)) ! Product of columns 1 and 2 of B
allocate(tAxmatrixB(columnas, columnas-1)) ! Transpose of A times B
allocate(max_vector(columnas)) ! Max values per row of B

! Creating matrix B -----------------------------------------------------------------------
do i = 1, columnas - 1
	matrixB(i,:) = matrixA(i,:)
end do

! Beginning with the exercises ------------------------------------------------------------
write(*, *) '--- Beginning with the exercise ---'
write(*, *) ''

! Variable initialization -----------------------------------------------------------------
traza = 0
no_traza = 0
col_impares = 0

! Check if the matrix is square so the product with itself is possible
if (columnas /= filas) then
	write(*, *) 'WARNING! The matrix is not a square matrix.'
	write(*, *) '> The trace of the matrix will not be calculated.'
	write(*, *) '> The sum of all non-diagonal elements of the matrix will not be calculated.'
	write(*, *) '> The square of the matrix will not be calculated.'
	write(*, *) ''
else
	! (a) Trace of the matrix <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	do i = 1, columnas
		traza = traza + matrixA(i, i)
	end do
	write(*, *) 'a) Writing output file for the trace of the matrix ...'
	call write_num(trim(results_dir) // '/1a.dat', traza)

	! (b) Sum of the non-diagonal elements <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	no_traza = sum(matrixA) - traza
	write(*, *) 'b) Writing output file for the sum of all non-diagonal elements of the matrix ...'
	call write_num(trim(results_dir) // '/1b.dat', no_traza)

	! (d) If it's a square matrix, compute the product of the matrix by itself <<<<<<<<<<<<
	allocate(matrix2(columnas, filas))
	matrix2 = transpose(matmul(transpose(matrixA), transpose(matrixA)))
	write(*, *) 'd) Writing output file for the square of the matrix ...'
	call write_mat(trim(results_dir) // '/1d.dat', matrix2)
end if

! (c) Sum of all elements in odd columns <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
do i = 1, columnas
	if (mod(i, 2) /= 0) then
		col_impares = col_impares + sum(matrixA(i,:))
	end if
end do

! (e) Transpose of the matrix <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
tmatrixA = transpose(matrixA)

! (f) Product of the transpose of the matrix and the matrix itself <<<<<<<<<<<<<<<<<<<<<<<<
tAxmatrixA = transpose(matmul(matrixA, tmatrixA))

! (g) Product of the matrix and the vector <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
if (dimvec /= columnas) then
	write(*, *) 'WARNING! The dimensions of the matrix and the vector do not match for a multiplication.'
else
	allocate(result_vec(filas))
	result_vec = matmul(transpose(matrixA), input_vec)
	write(*, *) 'g) Writing output file for the product of the matrix with the vector ...'
	call write_vec(trim(results_dir) // '/1g.dat', result_vec)
end if

! (h) Pointwise product of the first two columns of matrix B <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
point_vector = matrixB(1,:) * matrixB(2,:)

! (i) Product of the transpose of A and B <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
tAxmatrixB = transpose(matmul(matrixA, transpose(matrixB)))

! (j) Sum of all elements in B <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
suma_mat_b = sum(matrixB)

! (k) Maximum value in each column of matrix B <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
max_vector = maxval(matrixB, 1)

! Write all results -----------------------------------------------------------------------
write(*, *) 'c) Writing output file for the sum of all odd-column elements of the matrix ...'
call write_num(trim(results_dir) // '/1c.dat', col_impares)

write(*, *) 'e) Writing output file for the transpose of the matrix ...'
call write_mat(trim(results_dir) // '/1e.dat', tmatrixA)

write(*, *) 'f) Writing output file for the product of the transpose of the matrix with itself ...'
call write_mat(trim(results_dir) // '/1f.dat', tAxmatrixA)

write(*, *) 'h) Writing output file for the pointwise product of the two first columns of matrix B ...'
call write_vec(trim(results_dir) // '/1h.dat', point_vector)

write(*, *) 'i) Writing output file for the product of the transpose of the matrix with matrix B ...'
call write_mat(trim(results_dir) // '/1i.dat', tAxmatrixB)

write(*, *) 'j) Writing output file for the value of the sum of all elements in matrix B ...'
call write_int(trim(results_dir) // '/1j.dat', int(suma_mat_b))

write(*, *) 'k) Writing output file for the vector containing the maximum values per row of matrix B ...'
call write_vec(trim(results_dir) // '/1k.dat', max_vector)

! End of program --------------------------------------------------------------------------
write(*, *) ''
write(*, *) 'The program finished with no errors. You may go for a beer.'
write(*, *) ''
write(*, *) '--- End of program ---'

end program exercise1