
# Exercise 1 (0.1.0)

Last revision: Oct 15, 2021

---

This program solves the first exercise of the final project for the course\
*TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Installation

### a) Required software

This program has been compiled and tested using **GFortran**.\
Therefore, we suggest that you compile the source code using GFortran in a\
unix-like environment. If you don't have such an environment, please have a\
look at the [Installing GFortran](https://fortran-lang.org/learn/os_setup/install_gfortran)\
article in the Fortran official page.

To check if you have GFortran installed in your unix-like environment, please\
run the following command in a Terminal:

`which gfortran`

If the result isn't a path, please install GFortran using one of the following:

- Debian-based Linux: `sudo apt-get install gfortran`
- RedHat-based Linux: `sudo yum install gcc-gfortran` or\
`sudo dnf install gcc-gfortran`
- Arch Linux: `sudo pacman -S gcc-fortran`
- macOS: (if you have XCode installed) `xcode-select --install`\
otherwise `port search gcc && sudo port install gcc10`

### b) Source code

The source code for this program should be included with this README, and it\
should be comprised of 2 Fortran -f90- files:

- exercise1.f90
- rony_io.f90

The additional files and directories are used to compile the program, explain\
how to install and run the program, and to run some tests.

- **Matrices**
- **Vectors**
- README.txt
- makefile
- tests.sh

### c) GFortran and Make versions

Before compiling the program, it would be a good idea to check the version of\
GFortran and Make. This program was compiled and tested using GFortran 9.3.0 and\
GNU Make 4.2.1. To check your GFortran and Make versions, please run the following\
commands in a Terminal.

`gfortran --version`

`make --version`

### d) Installation

Finally, if you have GFortran installed in your system, and the version is\
comparable, then please open a Terminal window in the folder with the files,\
and run the following command:

`make`

Please note that the only files in each folder should be the ones listed in\
section 1. b) of this README. Otherwise, *make* may fail to compile the program.

Please note that this program uses subroutines. Therefore, if you wish to\
compile the program from the source code, I suggest that you first check the\
*makefile*.

Additionally, regarding the subroutines, it should be pointed out that these\
were designed to handle the input/output of the program, leaving the program\
with the Linear Algebra calculations. For more information, please check the\
source code.

## 2. Running the program

### a) Tests

Before running the program, some tests might be in order to make sure everything\
is working fine. To do so, please run the following command in a Terminal window\
inside the folder with the files:

`./tests.sh`

As a result, you should see 100 messages corresponding to all 100 tests you\
just made. If you see any error message stating that the program has FAILED,\
please try running the following command and start over with the installation\
as described in secion 1. d)

`make clean`

On the other hand, if you wish to check the results of each test, you may do\
so by going into the newly generated **Tests** directory and select one of the\
folders in it. Each one will contain the original matrix, the vector, plus all\
the results according to the exercise. To compare if all the results are in\
order, a **References** directory has been included in the main directory; it\
contains the results of running the `tests.sh` script successfully.

Finally, if you find any errors that you can't explain or fix by re-compiling\
the program, please contact the author; the details are provided at the start\
of this README.

### b) Input file formats

*Exercise 1* works by reading a **matrix.dat** file and a **vec.inp** file. The\
first one should be a matrix with entries as floating point numbers. It should\
not have a header or any additional information; just the matrix as is. Each row\
should be separated by a new line, and each row by a space. For example, a 4x4\
identity matrix should look like this:

1.0 0.0 0.0 0.0\
0.0 1.0 0.0 0.0\
0.0 0.0 1.0 0.0\
0.0 0.0 0.0 1.0

The vectors should follow the same format considering the fact that they are\
one dimensional matrices. For this reason, a vector in 3D should resemble the\
following:

1.0\
2.1\
3.2

This all means that if you choose to ignore one of these formats, the program\
will give you an error.

### c) Output files

*Exercise 1* will produce several output files inside a **results** folder\
depending on the provided matrix and vector. For instance, please consider\
that the trace of a matrix only makes sense for a square matrix. And\
matrix-matrix or matrix-vector multiplications only make sense if the number\
of columns of the former matches the the number of rows of the latter.

That being said, the output files will be in text format and will have a `.dat`\
file extension. If the result of a given calculation is a matrix, then the\
output file will contain a matrix. If the result is a vector, then the output\
file will be a vector. Finally, if the result is a number, the file will only\
contain a number. For a detailed description of each task in the program and\
the output file's name, please refer to section 2. f) in this README.

### d) Running it

Once you have our matrix in a **matrix.dat** file, and your vector in a\
**vec.inp** file, you may proceed to move them both to the same folder where\
**exercise1.exe** is located and run the following command:

`./exercise1.exe`

If you get any error from running this program, please refer to sections\
2. a) and 2. b). Otherwise, please contact the author.

### e) Example

To run a quick test, it is suggested that you try the following:

1. Create a new empty file called `matrix.dat`
2. Open the file and fill it with the following matrix

2.5 -3.2 -0.2\
-1.8 3.0 -1.5\
-2.1 -2.9 1.7

3. Save the file and close it
4. Create a new empty file called `vec.inp`
5. Open the file and fill it with the following vector

0.22\
-0.73\
0.16

6. Save the file and close it
7. Make sure that both files are in the same directory as `exercise1.exe`
8. Open a Terminal window and change your directory to the one with the files\
and the program.
9. Run the program by typing the following in the Terminal and pressing enter:

`./exercise1.exe`

10. The program should give some feedback about the exercises it carried out,\
but the most important line is the one telling you that:

`The program finished with no errors.`

Additionally, the program should have created a new directory called **results**.\
In it, you should find all the results of running the program with this combination\
of matrix and vector through each exercise. To check if the program has carried out\
everything correctly, an **Example** directory has been included for you to compare\
your results.

### f) Theory behind it

The program is designed to carry out a specific set of tasks involving matrix\
algebra and manipulations. To do this, the read/write operations where separated\
in a module (_rony_io.f90_) so that the main program (_exercise1.f90_) held only\
the code for the required tasks.

The program opens a file _matrix.dat_ and read its contents. (The matrix is read\
transposed, but all operations in the code are carried out taking this into\
consideration.) The latter should be a square or a rectangular matrix. The program\
will read the matrix by counting the number of lines and the number of columns of\
the first row, and build a matrix **A** out of both dimensions and each entry.\
A similar procedure is followed to read the vector _vec.inp_. Next, a matrix **B** is\
created by copying all columns of **A** except the last one. Then, the following\
tasks are carried out:

* (a) Compute the trace of the matrix (if said matrix is square) `trace(A)`
> A simple `do` loop was used to compute the trace.\
> **Ouptut file:** _1a.dat_

* (b) Compute the sum of all non-diagonal elements (if the matrix is square)
> Computing the sum of all elements in the matrix through `SUM()` and then\
> subtracting the trace was the simplest path.\
> **Ouptut file:** _1b.dat_

* (c) Compute the sum of all elements in odd columns of the matrix
> To do this, a `do` loop, an `if` statement and a modulus function `MOD()`\
> were used to go through all columns and check if they were even or odd.\
> **Ouptut file:** _1c.dat_

* (d) Compute the square of the matrix (if said matrix is square) `A²`
> This was easily achieved using `MATMUL()`\
> **Ouptut file:** _1d.dat_

* (e) Compute the transpose of the matrix `(A)^T`
> This was also achieved easily through `TRANSPOSE()`\
> **Ouptut file:** _1e.dat_

* (f) Compute the product of the transpose of the matrix and the matrix `(A)^T · A`
> Using the result from _(e)_, the matrices were multiplied with the same\
> function as the one described for _(d)_.\
> **Ouptut file:** _1f.dat_

* (g) Compute the matrix-vector product `A · v`
> Again, this was the same function as in _(d)_, but with a vector as the\
> second entry.\
> **Ouptut file:** _1g.dat_

* (h) Compute the pointwise product of the first two columns of `B`
> Extracting the first two columns through slicing, a simple multiplication\
> of the two vectors was done.\
> **Ouptut file:** _1h.dat_

* (i) Compute the matrix-matrix product of the transpose of **A** and **B** `(A)^T · B`
> The same procedure as _(f)_ was used.\
> **Ouptut file:** _1i.dat_

* (j) Compute the sum of all elements of **B**
> This was achieved, again, by using the function `SUM()`, as described in _(b)_.\
> **Ouptut file:** _1j.dat_

* (k) Create a vector with contains the maximum value within each row of **B**
> This task was carried out by using the function `MAXVAL()`, and specifying\
> the required dimension.\
> **Ouptut file:** _1k.dat_

Finally, please consider that each result is stored as an individual output\
file, inside a folder named **results**. This folder is created by the program\
at runtime. The files are all named according to the aforementioned tasks;\
the starting letter is the key to match the result with the task. For your\
convenience, the name of each Ouptut file is included after each of the\
previous descriptions.

## 3. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin or enable\
the WSL environment, you should be able to compile and run the program. However,\
we do suggest that you have a look at section 1. a) of this README. GNU Fortran\
is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file, please\
follow this link to locate the project's repository: [https://gitlab.com/zronyj/emtccm_fortran_e1](https://gitlab.com/zronyj/emtccm_fortran_e1)

3. *The compilation failed and I have a lot of strange files in my folder.* - In\
this case, please proceed to remove all files with `.o` and `.mod` file extensions\
and try to compile the program again. If this fails, please check sections 1. a)\
and 1. c) of this README.

4. *How large can my matrix/vector be?* - We have tested the program with matrices\
and vectors up to 10 dimensions. In theory, the program should be able to handle\
larger matrices/vectors; but it will depend on the size of each number, and the\
number of decimals. If you try with larger matrices, let us know! If you find an\
error, please let us know as well.

5. *Can the program read CSV files?* - CSV files are usually separated by commas\
and have a **.csv** file extension. This goes against the formats described\
in section 2. b). Therefore, **exercise1** is not intended to read CSV files.

## 4. Acknowledgements

Special thanks go to *Ania Beatriz Rodriguez* and *Orla Mary Gleeson* for making\
me laugh and making my evenings so entertaining while writing all this code.

## 5. License

Copyright 2021 Rony J. Letona

Permission is hereby granted, free of charge, to any person obtaining a copy of\
this software and associated documentation files (the "Software"), to deal in the\
Software without restriction, including without limitation the rights to use, copy,\
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,\
and to permit persons to whom the Software is furnished to do so, subject to the\
following conditions:

The above copyright notice and this permission notice shall be included in all\
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL\
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING\
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS\
IN THE SOFTWARE.