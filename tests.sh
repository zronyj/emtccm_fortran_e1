#!/bin/bash

mkdir Tests
for m in `seq 0 9`
do
	for v in `seq 0 9`
	do
		dname="mat$m-vec$v"
		mkdir $dname
		cp ./exercise1.exe ./$dname/exercise1.exe
		cp ./Matrices/matrix$m.dat ./$dname/matrix.dat
		cp ./Vectors/vec$v.inp ./$dname/vec.inp
		cd $dname
		if ./exercise1.exe > testM$m-V$v.log
		then
			echo -e "\n----------------------------------------------------------"
			echo "* Test with matrix$m.dat and vector$v.inp was successful!"
			echo "----------------------------------------------------------"
		else
			echo -e "\n----------------------------------------------------------"
			echo "X Test with matrix$m.dat and vector$v.inp FAILED!"
			echo "X Please check the log file in $dname"
			echo "----------------------------------------------------------"
		fi
		rm exercise1.exe
		cd ../
		mv $dname ./Tests/
	done
done