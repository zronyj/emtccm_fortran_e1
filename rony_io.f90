module rony_io

implicit none

! Establishing the subroutines which will be provided to the external program
public :: read_mat, read_vec, write_mat, write_vec, write_num, write_int

contains

	subroutine read_vec(vec_fil_nam, my_vec)
	! Special subroutine to read vectors from files ---------------------------------------
	! Inputs
	! - vec_fil_nam: name of the file containing the vector to be read
	! Outputs
	! - my_vec: an array with the data from the vector file
	! Description
	! The subroutine opens a file, counts the number of rows and, with that information, it
	! proceeds to read each row as an entry from the vector. It then closes the file.
	! -------------------------------------------------------------------------------------

		implicit none
		
		! Defining variables
		
		! The ones going in and out
		character(*), intent(in) :: vec_fil_nam
		real, allocatable, intent(out) :: my_vec(:)
		
		! The ones staying inside
		character(len=10) :: buf
		integer :: dimvec, stat, i, j
		
		! Open the file
		open(42, file=vec_fil_nam, status='old')
		
		! Counting how many rows does the vector have
		dimvec = 0
		do
			read(42, *, iostat=stat) buf
			if (stat < 0) exit
			dimvec = dimvec + 1
		end do
		rewind(42)
		
		! Memory allocation for vector
		allocate(my_vec(dimvec))
		
		! Read the vector's entries
		read(42, *) my_vec(:)
		
		! Close the file
		close(42)

	end subroutine read_vec

	subroutine read_mat(mat_fil_nam, my_mat)
	! Special subroutine to read matrices from files --------------------------------------
	! Inputs
	! - mat_fil_nam: name of the file containing the matrix to be read
	! Outputs
	! - my_mat: an array with the data from the matrix file
	! Description
	! The subroutine opens a file, counts the number of rows and, by detecting spaces, it
	! counts the number of columns in the matrix. It then proceeds to read each row as a
	! row of the matrix. It then closes the file.
	! -------------------------------------------------------------------------------------

		implicit none
		
		! Defining variables
		
		! The ones going in and out
		character(*), intent(in) :: mat_fil_nam
		real, allocatable, intent(out) :: my_mat(:,:)
		
		! The ones staying inside
		character(len=1000) :: leer, buf
		integer :: columnas, filas, stat, i, j
		
		! Open the file
		open(40, file=mat_fil_nam, status='old')
		
		! Read the first line of the file as text
		read(40, '(a)') leer
		
		! Count the number of rows
		filas = 1
		do
			read(40, *, iostat=stat) buf
			if (stat < 0) exit
			filas = filas + 1
		end do
		
		! Check for the number of columns by analyzing spaces in the first line of text
		do i = 1, len(trim(leer))-1
			if (i == 1) then
				if (leer(i:i) /= ' ') then ! If the first character is not blank ...
					columnas = 1 ! ... then I have my first column!
				else
					columnas = 0 ! If not, I still have no column.
				end if
			else ! If it's not the first charactar, it's not blank, but the previous one is ...
				if ((leer(i-1:i-1) == ' ') .and. (leer(i:i) /= ' ')) then
					columnas = columnas + 1 ! ... I have a new column!
				end if
			end if
		end do

		! Memory allocation for the matrix
		allocate(my_mat(columnas, filas))
		rewind(40)
		
		! Read the matrix' entries
		read(40, *) ((my_mat(i,j), i=1, columnas), j=1, filas)
		
		! Close the file
		close(40)

	end subroutine read_mat

	subroutine write_int(int_fil_nam, my_int)
	! Special subroutine to write integers to files ---------------------------------------
	! Inputs
	! - int_fil_nam: name of the file to write the integer to
	! - my_int: integer value to be written into the file
	! Outputs
	! - the file with name int_fil_nam and contents my_int
	! Description
	! The subroutine opens a file, writes an integer into it, and closes the file.
	! -------------------------------------------------------------------------------------

		implicit none
		
		! Defining variables
		
		! The ones going in
		character(*), intent(in) :: int_fil_nam
		integer, intent(in) :: my_int
		
		! Open the file
		open(56, file=int_fil_nam, status='replace')
		
		! Write the integer
		write(56, *) my_int
		
		! Close the file
		close(56)

	end subroutine write_int

	subroutine write_num(num_fil_nam, my_num)
	! Special subroutine to write scalars to files ----------------------------------------
	! Inputs
	! - num_fil_nam: name of the file to write the number in decimal form to
	! - my_num: number in decimal form value to be written into the file
	! Outputs
	! - the file with name num_fil_nam and contents my_num
	! Description
	! The subroutine opens a file, writes an number in decimal form into it, and closes the
	! file.
	! -------------------------------------------------------------------------------------

		implicit none
		
		! Defining variables
		
		! The ones going in
		character(*), intent(in) :: num_fil_nam
		real, intent(in) :: my_num
		
		! Open the file
		open(54, file=num_fil_nam, status='replace')
		
		! Write the number (as a real number)
		write(54, *) my_num
		
		! Close the file
		close(54)

	end subroutine write_num

	subroutine write_vec(vec_fil_nam, my_vec)
	! Special subroutine to write vectors to files ----------------------------------------
	! Inputs
	! - vec_fil_nam: name of the file to write the vector to, as a column
	! - my_vec: vector, in decimal form, to be written into the file
	! Outputs
	! - the file with name vec_fil_nam and contents my_vec
	! Description
	! The subroutine opens a file, writes the elements of a vector (as a column) into it,
	! and closes the file.
	! -------------------------------------------------------------------------------------

		implicit none
		
		! Defining variables
		
		! The ones going in
		character(*), intent(in) :: vec_fil_nam
		real, allocatable, intent(in) :: my_vec(:)
		
		! Open the file
		open(52, file=vec_fil_nam, status='replace')
		
		! Write the vector as a single column with real entries
		write(52, "(1F15.7)") my_vec
		
		! Close the file
		close(52)

	end subroutine write_vec

	subroutine write_mat(mat_fil_nam, my_mat)
	! Special subroutine to write matrices to files ---------------------------------------
	! Inputs
	! - mat_fil_nam: name of the file to write the matrix to
	! - my_mat: matrix, in decimal form, to be written into the file
	! Outputs
	! - the file with name mat_fil_nam and contents my_mat
	! Description
	! The subroutine opens a file, writes the elements of a matrix (as an array) into it,
	! and closes the file.
	! -------------------------------------------------------------------------------------

		implicit none
		
		! Defining variables
		
		! The ones going in
		character(*), intent(in) :: mat_fil_nam
		character(len=100) :: dim
		real, allocatable, intent(in) :: my_mat(:,:)
		
		! The ones staying inside
		integer :: columnas
		columnas = size(my_mat, 1)
		
		! Transforming the number into a string
		write(dim, *) columnas
		
		! Open the file
		open(50, file=mat_fil_nam, status='replace')
		
		! Write the matrix as an array with 'columnas' number of columns
		write(50, "(" // trim(dim) // "F15.7)") my_mat
		
		! Close the file
		close(50)

	end subroutine write_mat


end module rony_io