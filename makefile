# Makefile for Exercise 1

exercise1: rony_io.o exercise1.o
	gfortran exercise1.o rony_io.o -o exercise1.exe
	chmod +x exercise1.exe
	chmod +x tests.sh
	mkdir source && mv *.f90 ./source/
	rm -f *.o
	rm -f *.mod

exercise1.o: exercise1.f90
	gfortran -c exercise1.f90 -o exercise1.o

rony_io.o: rony_io.f90
	gfortran -c rony_io.f90 -o rony_io.o

.PHONY: clean

clean:
	mv source/*.f90 ./
	rmdir source
	rm *.exe